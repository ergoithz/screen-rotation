# screen-rotation

This is an screen rotation daemon using `xinput`, `xrandr` and `udev`.

## Installation

Ensure `python-pyudev` is installed.

Copy `screen-rotation` to somewhere on `PATH`, for example `/usr/local/bin`.

```sh
sudo cp screen-rotation /usr/local/bin
```

## Setup

A config file will be created at `~/.config/screen-rotation/config.conf`, 
during first run, it is well docummented, add your docking devices (which will
lock the rotation when connected), pointer devices (which will rotate alongside
your screen), your rotation order (depending on your device, experiment
with it) and accelerometer device.

## xfce4-genmon-plugin

Create a xfce4-genmon-plugin on your xfce4-panel, set the command below.

```sh
screen-rotation genmon
```

You can also set the refresh interval to a high value (like 1 hour) since
`screen-rotation` will actively update its calling genmon applets via
`xfce-panel` events.

![xfce4-genmon-plugin screen-rotation properties](./docs/genmon-properties.png)

The applet should look something like this:

![xfce4-genmon-plugin screen-rotation tooltip](./docs/genmon-tooltip.png)